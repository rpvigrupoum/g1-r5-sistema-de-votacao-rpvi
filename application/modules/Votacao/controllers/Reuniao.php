<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reuniao extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        header('Cache-Control: no cache');
    }

    public function index()
    {
        $this->load->library('session');
        $this->load->model("reuniao_model");
        $lista = $this->reuniao_model->buscaPeloModerador($this->session->userdata('matricula'));
        $lista2 = $this->reuniao_model->buscaTodasComoConselheiro($this->session->userdata('matricula'));

        $dados = array("reuniao" => $lista,
            "reunioesComoConselheiros" => $lista2);



        $this->load->view('template/header');
        $this->load->view('template/nav-top');
        $this->load->view('lista_reunioes', $dados);
        $this->load->view('template/footer');
        $this->load->view('template/js');
    }

    public function opcao()
    {
        $id = $this->input->get("id");
        $this->load->model("item_pauta_model");
        $itemPauta = $this->item_pauta_model->retorna($id);

        $this->load->model("opcao_voto_model");
        $opcaoVoto = $this->opcao_voto_model->retorna($id);

        $dados = array("itemDePauta" => $itemPauta,
            "opcoesVoto" => $opcaoVoto);

        $this->load->view('opcao', $dados);
    }

    public function visualizarReunioesAbertas()
    {
        header('Cache-Control: no-cache');
        header("Content-Type: text/event-stream\n\n");

        $this->load->model("reuniao_model");
        $reunioesAbertas = $this->reuniao_model->buscarReunioesAbertas();
        
        echo "event: reunioesAbertas\n";
        echo 'data: {';
        
        foreach($reunioesAbertas as $reuniao){
            echo 'id:'.$reuniao["id_reuniao"].',';
        }
        
        echo "}";
        echo "\n\n";
        ob_end_flush();
        flush();
    }

    public function alterarStatus()
    {
        $id = $this->input->post("id_reuniao");
        $status = $this->input->post("status");

        $this->load->model("reuniao_model");
        $this->reuniao_model->alterarStatus($id, $status);
    }

}
