<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ItemPauta extends CI_Controller {

    public function __construct() {
        parent::__construct();
        header('Cache-Control: no cache');
    }

    public function index() {
        $id = $this->input->post("id");
        $this->load->model("reuniao_model");
        $reuniao = $this->reuniao_model->retorna($id);

        $this->load->model("item_pauta_model");
        $lista = $this->item_pauta_model->buscaItensPeloId($id);

        $dados = array("reuniao" => $reuniao,
            "item_pauta" => $lista);

        $this->load->view('template/header');
        $this->load->view('template/nav-top');
        $this->load->view('lista_itemPautas', $dados);
        $this->load->view('template/footer');
        $this->load->view('template/js');
    }

    public function opcao() {
        $id = $this->input->post("id");
        $this->load->model("item_pauta_model");
        $itemPauta = $this->item_pauta_model->retorna($id);

        $this->load->model("opcao_voto_model");

        if ($itemPauta['tipoVotacao'] == 2) {
            $opcaoVoto = $this->opcao_voto_model->retorna($id);
            $dados = array("itemDePauta" => $itemPauta,
                "opcoesVoto" => $opcaoVoto);

            $this->load->view('opcao', $dados);
        } else {
            $opcaoVoto = array();
            $dados = array("itemDePauta" => $itemPauta,
                "opcoesVoto" => $opcaoVoto);

            $this->load->view('opcao', $dados);
        }
    }

}
