<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class VotacaoMembro extends CI_Controller {

    public function __construct() {
        parent::__construct();
        header('Cache-Control: no cache');
    }

    public function votacao() {
        $id = $this->input->post("id_item_pauta");
        $this->load->model("opcao_voto_model");
        $this->load->model("item_pauta_model");
        $ItemPauta = $this->item_pauta_model->retorna($id);
        $opcoes = array("opcoes" => $this->opcao_voto_model->retorna($id),
            "ItemPauta" => $ItemPauta);

        $this->load->view('template/header');
        $this->load->view('template/nav-top');
        $this->load->view('votar', $opcoes);
        $this->load->view('template/footer');
        $this->load->view('template/js');
    }

    public function votar() {
        $this->load->library('session');
        $id = $this->input->post("id_item_pauta");
        $matricula = $this->session->userdata('matricula');
        $id_opcao_voto = $this->input->post("id_opcao_voto");
        $this->load->model("votacao_membro_model");
        $voto = ['id_item_pauta' => $id, 'matricula' => $matricula, 'id_opcao_voto' => $id_opcao_voto];
        $result = $this->votacao_membro_model->salvarVoto($voto);
        redirect("reuniao");  
    }

    public function deleteVotos($id) {
        $id = $this->input->post("id_item_pauta");
        $this->load->model("votacao_membro_model");
        $this->votacao_membro_model->removerVotos($id);
    }

    public function aguardarVotacao() {
        $id = $this->input->post("id_reuniao");
        $array = ['id_reuniao' => $id];

        $this->load->view('template/header');
        $this->load->view('template/nav-top');
        $this->load->view('aguardar_votacao', $array);
        $this->load->view('template/footer');
        $this->load->view('template/js');
    }

}
