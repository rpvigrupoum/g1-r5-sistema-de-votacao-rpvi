<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class OpcaoVoto extends CI_Controller {

    public function index() {
        $this->load->helper('url');
        $this->load->helper('funcoes');
        load_css('boostrap');
    }

    public function cadastrarSimples() {
        $id = $this->input->post('idItem');

        $this->deleteOpcao($id);
        $this->setTipoVoto($id, 1);
        $this->db->where('id_item_pauta', $id);
        $data['tipoVotacao'] = 1;
        $resultado = $this->db->update('item_pauta', $data);
        $favor['id_item_pauta'] = $id;
        $favor['descricao']='A favor';
        $contra['id_item_pauta']=$id;
        $contra['descricao']='Contrário';
        $abs['id_item_pauta']=$id;
        $abs['descricao']='Abstenção';
         $this->db->insert('opcao_voto', $favor);
         $this->db->insert('opcao_voto', $contra);
         $this->db->insert('opcao_voto', $abs);
    }

    public function cadastrarPerso() {

        $array = $_POST['arrayOpcao'];
        $opcoes = explode(',', $array[0]);

        $id = $this->input->post('idItem');
        $this->deleteOpcao($id);
        $this->setTipoVoto($id, 2);
        $data['id_item_pauta'] = $this->input->post('idItem');

        for ($i = 0; $i < count($opcoes); $i++) {
            $data['id_item_pauta'] = $this->input->post('idItem');
            $data['descricao'] = $opcoes[$i];
            $resultado = $this->db->insert('opcao_voto', $data);
        }
        if ($resultado) {
            $this->session->set_flashdata("success", "Cadastro realizado com sucesso!");
        } else {
            $this->session->set_flashdata("danger", "Erro no cadastramento!");
        }
    }

    public function deleteOpcao($id) {
        $this->db->delete('opcao_voto', ['id_item_pauta' => $id]);
        $q = $this->db->where('id_item_pauta', $id)->get('voto');
        foreach ($q->result() as $Child) {
            $this->deleteOpcao($Child->id_item_pauta);
        }
    }

    public function setTipoVoto($id, $tipo) {
        $this->db->where('id_item_pauta', $id);
        $data['tipoVotacao'] = $tipo;
        $this->db->update('item_pauta', $data);
    }

    public function iniciaVotacao() {
        $this->load->model("opcao_voto_model");
        $this->load->model("reuniao_model");
        $this->load->model("item_pauta_model");
        $id = $this->input->post('idItem');
        
            $listaOpcoes = $this->db->get_where("opcao_voto", array(
                        "id_item_pauta" => $id
                    ))->result_array();
            $tamanho = sizeof($listaOpcoes);
            if ($tamanho <= 1) {
                $this->session->set_flashdata("danger", "Devem haver no mínimo duas opções cadastradas!");
                redirect(base_url('/ItemPauta/opcao?id=' . $id));
            } else {
                $this->db->where('id_item_pauta', $id);
                $idReuniao = $this->item_pauta_model->retornaIdReunião($id);

                $this->reuniao_model->alterarStatus($idReuniao, 'fechado');
                $data['votacao_iniciada'] = 1;
                $this->db->where('id_item_pauta', $id);
                $this->db->update('item_pauta', $data);
                $this->session->set_flashdata("success", "Votação iniciada!");


                $itemPautaVotacao = $this->item_pauta_model->retorna($id);
                $opcaoVotoVotacao = $this->opcao_voto_model->retorna($id);
                $dados = array("itemDePautaVotacao" => $itemPautaVotacao,
                    "opcoesVoto" => $opcaoVotoVotacao);
                $this->load->view('em_votacao', $dados);
            }
         
    }

    public function sentVotacao() {
        header('Cache-Control: no-cache');
        header("Content-Type: text/event-stream\n\n");

        $this->load->model("item_pauta_model");
        $itemVotacao = $this->item_pauta_model->retornaVotacoesAbertas();
        echo "event: aguardarVotacao\n";
        echo 'data: {';
        foreach ($itemVotacao as $item) {
            echo 'idReuniao:' . $item['id_reuniao'] . ',';
            echo 'idItemPauta:' . $item['id_item_pauta'] . ';';
        }
        echo "}";
        echo "\n\n";
        ob_end_flush();
        flush();
    }

}
