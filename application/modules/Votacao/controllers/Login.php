<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        header('Cache-Control: no cache');
    }

    public function index()
    {
        $this->load->view('index');
    }

    public function autenticar()
    {
        $this->load->library('session');
        $this->load->model('usuario_model');
        $matricula = $this->input->post('matricula');
        $senha = $this->input->post('senha');
        $usuario = $this->usuario_model->logarUsuarios($matricula, $senha);
        if($usuario){
            $this->session->set_userdata($usuario);
            redirect("reuniao");           
        }else{
            redirect();        
        }
    }

}
