<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class VotacaoModerador extends CI_Controller {

    
    public function finalizaVotacao() {
        $idItem = $this->input->post('id_item_pauta'); 
        $this->load->model('item_pauta_model');
        $votacao = $this->item_pauta_model->retornaVotacao($idItem);
        $descricoes=array();
        $matriculas=array();
        foreach($votacao as $voto){
            $descricao = $this->item_pauta_model->retornaDescricaoOpcao($voto['id_opcao_voto']);
            $matricula = $voto['matricula'];
            array_push($descricoes,$descricao);
            array_push($matriculas,$matricula);
                
        }
        $idReuniao = $this->item_pauta_model->retornaIdReunião($idItem);
        
        $dados= array('matriculas'=>$matriculas, 'descricoes'=>$descricoes, 'idReuniao'=>$idReuniao);
       $this->load->view('relatorio_votacao', $dados);
       
        $this->item_pauta_model->fechaVotacao($idItem);
    }
 
            
}