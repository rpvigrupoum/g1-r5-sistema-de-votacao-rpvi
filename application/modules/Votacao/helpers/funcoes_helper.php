<?php

function load_css($arquivo=null,$media='screen', $import=FALSE, $echo= TRUE)
{
    $css='';
    if($import == TRUE){
        $css = '<link rel="stylesheet" type="text/css" href="'.$arquivo.'" media="'.$media.'" />'."\n";
    } else{
        $css = '';
        if($arquivo != NULL){
            if(file_exists('./assets/css/'.$arquivo.'.css')){
                $css = base_url('assets/css/'.$arquivo.'.css');
            }
            if($echo == TRUE){
                echo '<link rel="stylesheet" type="text/css" href="'.$css.'" media="'.$media.'"/>'."\n";
            }else{
                return '<link rel="stylesheet" type="text/css" href="'.$css.'" media="'.$media.'" \>'."\n";
            }
    } else{
        echo 'arquivo não encontrado';
    }   
    }
}

function load_js($arquivo=NULL, $echo=TRUE){
    if($arquivo != NULL){
        if(file_exists('./assets/js/'.$arquivo.'.js'));
    }
    $js = '<script tye="text/javascript" src="'.$arquivo.'"></script>'."\n";
    if($echo):
        echo $js;
    else:
        return $js; 
    endif;
}