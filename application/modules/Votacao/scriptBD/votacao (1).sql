-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 03-Dez-2018 às 19:58
-- Versão do servidor: 10.1.35-MariaDB
-- versão do PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `votacao`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `item_pauta`
--

CREATE TABLE `item_pauta` (
  `id_item_pauta` int(11) NOT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `id_reuniao` int(11) NOT NULL,
  `tipoVotacao` int(50) DEFAULT NULL,
  `votacao_iniciada` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `item_pauta`
--

INSERT INTO `item_pauta` (`id_item_pauta`, `descricao`, `id_reuniao`, `tipoVotacao`, `votacao_iniciada`) VALUES
(1, 'Pauta para inserir fisica na grade de engenharia de software', 1, 2, 1),
(2, 'Pauta para decidir se Calculo não essencial para o curso de Engenharia de Software', 1, 2, 1),
(3, 'testando ', 2, 2, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `opcao_voto`
--

CREATE TABLE `opcao_voto` (
  `id_opcao_voto` int(11) NOT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `id_item_pauta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `opcao_voto`
--

INSERT INTO `opcao_voto` (`id_opcao_voto`, `descricao`, `id_item_pauta`) VALUES
(88, 'Descricao1,Descricao2', 2),
(89, '1,2,3', 3),
(99, 'aa', 1),
(100, 'aass', 1),
(101, 'asas', 1),
(102, 'aasasass', 1),
(103, 'asss', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `reuniao`
--

CREATE TABLE `reuniao` (
  `id_reuniao` int(11) NOT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `moderador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `reuniao`
--

INSERT INTO `reuniao` (`id_reuniao`, `descricao`, `status`, `moderador`) VALUES
(1, 'Modificação do PPC do curso de Eng. de Software', 'aberto', 161020013),
(2, 'Apresentação de Artigos na UNIPAMPA', 'fechado', 161020013);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `matricula` int(11) NOT NULL,
  `senha` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`matricula`, `senha`) VALUES
(161020013, '123456'),
(161152140, '1234');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario_has_membro_reuniao`
--

CREATE TABLE `usuario_has_membro_reuniao` (
  `usuario_matricula` int(11) NOT NULL,
  `reuniao_id_reuniao` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario_has_membro_reuniao`
--

INSERT INTO `usuario_has_membro_reuniao` (`usuario_matricula`, `reuniao_id_reuniao`) VALUES
(161020013, 2),
(161152140, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `item_pauta`
--
ALTER TABLE `item_pauta`
  ADD PRIMARY KEY (`id_item_pauta`),
  ADD KEY `fk_item_pauta_reuniao1_idx` (`id_reuniao`);

--
-- Indexes for table `opcao_voto`
--
ALTER TABLE `opcao_voto`
  ADD PRIMARY KEY (`id_opcao_voto`),
  ADD KEY `fk_opcao_voto_item_pauta1_idx` (`id_item_pauta`);

--
-- Indexes for table `reuniao`
--
ALTER TABLE `reuniao`
  ADD PRIMARY KEY (`id_reuniao`),
  ADD KEY `fk_reuniao_usuario1_idx` (`moderador`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`matricula`);

--
-- Indexes for table `usuario_has_membro_reuniao`
--
ALTER TABLE `usuario_has_membro_reuniao`
  ADD PRIMARY KEY (`usuario_matricula`,`reuniao_id_reuniao`),
  ADD KEY `fk_usuario_has_reuniao_reuniao1_idx` (`reuniao_id_reuniao`),
  ADD KEY `fk_usuario_has_reuniao_usuario1_idx` (`usuario_matricula`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `item_pauta`
--
ALTER TABLE `item_pauta`
  MODIFY `id_item_pauta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `opcao_voto`
--
ALTER TABLE `opcao_voto`
  MODIFY `id_opcao_voto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `reuniao`
--
ALTER TABLE `reuniao`
  MODIFY `id_reuniao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `item_pauta`
--
ALTER TABLE `item_pauta`
  ADD CONSTRAINT `fk_item_pauta_reuniao1` FOREIGN KEY (`id_reuniao`) REFERENCES `reuniao` (`id_reuniao`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `opcao_voto`
--
ALTER TABLE `opcao_voto`
  ADD CONSTRAINT `fk_opcao_voto_item_pauta1` FOREIGN KEY (`id_item_pauta`) REFERENCES `item_pauta` (`id_item_pauta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `reuniao`
--
ALTER TABLE `reuniao`
  ADD CONSTRAINT `fk_reuniao_usuario1` FOREIGN KEY (`moderador`) REFERENCES `usuario` (`matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `usuario_has_membro_reuniao`
--
ALTER TABLE `usuario_has_membro_reuniao`
  ADD CONSTRAINT `fk_usuario_has_reuniao_reuniao1` FOREIGN KEY (`reuniao_id_reuniao`) REFERENCES `reuniao` (`id_reuniao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuario_has_reuniao_usuario1` FOREIGN KEY (`usuario_matricula`) REFERENCES `usuario` (`matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
