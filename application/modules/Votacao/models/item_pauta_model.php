<?php

class Item_pauta_model extends CI_Model {

    public function buscaTodas() {
        return $this->db->get("item_pauta")->result_array();
    }
    public function buscaItensPeloId($id){
        return $this->db->get_where("item_pauta", array('id_reuniao' => $id))->result_array();
    }
    public function retorna($id) {
        return $this->db->get_where("item_pauta", array(
                    "id_item_pauta" => $id
                ))->row_array();
    }


    public function retornaVotacoesAbertas() {
        return $this->db->get_where("item_pauta", array(
                    "votacao_iniciada" => 1
                ))->result_array();
    }

    public function retornaIdReunião($idItem) {
        $itemPauta = $this->db->get_where("item_pauta", array(
                    "id_item_pauta" => $idItem
                ))->row_array();
        return $itemPauta['id_reuniao'];
    }

    public function retornaVotacao($id) {
        return $this->db->get_where("voto", array(
                    "id_item_pauta" => $id
                ))->result_array();
    }

    public function retornaDescricaoOpcao($id_opcao) {
        return $this->db->get_where("opcao_voto", array(
                    "id_opcao_voto" => $id_opcao
                ))->row_array();
    }

    public function fechaVotacao($idItem) {
       $this->db->where('id_item_pauta', $idItem);
        $data['votacao_iniciada'] = 0;
        $resultado = $this->db->update('item_pauta', $data);
    }

}
