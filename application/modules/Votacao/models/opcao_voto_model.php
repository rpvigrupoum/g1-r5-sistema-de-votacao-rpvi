<?php

class Opcao_voto_model extends CI_model {

    public function salva($opcao_voto) {
        $this->db->insert("opcao_voto", $opcao_voto);
    }

    public function retorna($id) {
       
            return $this->db->get_where("opcao_voto", array(
                        "id_item_pauta" => $id
                    ))->result_array();
         
    }

}
