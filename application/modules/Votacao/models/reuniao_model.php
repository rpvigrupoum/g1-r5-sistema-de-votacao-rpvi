<?php

class Reuniao_model extends CI_Model {

    public function buscaTodas()
    {
        return $this->db->get("reuniao")->result_array();
    }
    public function buscaPeloModerador($matricula){
        return $this->db->get_where("reuniao", array('moderador'=>$matricula))->result_array();
    }
    public function buscaTodasComoConselheiro($matricula)
    {
        $reunioesId = $this->db->get_where("usuario_has_membro_reuniao",
                array('usuario_matricula' => $matricula))->result_array();
        $reunioes = array();
        foreach ($reunioesId as $reuniaoId) {
            array_push($reunioes, $this->retorna($reuniaoId["reuniao_id_reuniao"]));
        }
        return $reunioes;
    }

    public function buscarReunioesAbertas()
    {
        return $this->db->get_where("reuniao", array(
                    "status" => "aberto"
                ))->result_array();
    }

    public function retorna($id)
    {
        return $this->db->get_where("reuniao", array(
                    "id_reuniao" => $id
                ))->row_array();
    }

    public function alterarStatus($id, $status)
    {
        $this->db->where("id_reuniao", $id);
        $this->db->set("status", $status);
        $this->db->update("reuniao");
    }

}
