<?php

class votacao_membro_model extends CI_Model {

    public function buscaTodas() {
        return $this->db->get("item_pauta")->result_array();
    }

    public function salvarVoto($voto) {
        $this->db->insert("voto", $voto);
    }

    public function removerVotos($id_item_pauta) {
        $this->db->where('id_item_pauta', $id_item_pauta);
        $this->db->delete('voto');

        return true;
    }
    

}
