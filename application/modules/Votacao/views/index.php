<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Login</title>
        <!--        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
                <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>-->
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href='https://guri.unipampa.edu.br/public/themes/moder//css/estilo_menu.css' type="text/css" />
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <style>
            a, li, .form-group, th, .btn, p, .form-control, .panel-default{
                font-size: 14px;
            }
            
            .novo_logar{
                width:100%;
                margin-right: 0px;
            }
            
            .novo_input-login{
                width: 100%;
            }
        </style>
    </head>
    <body>
                <div class="reset">    
            <div id="novo_tarja-topo"></div>
            <div id="novo_topo">
                <h1 id="novo_guri-marca">
                    <a href="https://guri.unipampa.edu.br/" title="GURI">
                        <img id="novo_marca" src="https://guri.unipampa.edu.br/public/themes/moder/imgs/logo_guri.png" alt="GURI"/>
                    </a>            
                </h1>
                <h2 id="novo_unipampa">
                    <a href="http://www.unipampa.edu.br" title="Acessar Portal da UNIPAMPA" target="_new" >
                        <img class="novo_marca-unipampa" src="https://guri.unipampa.edu.br/public/themes/moder/imgs/marca.png" alt="Acessar Portal da UNIPAMPA"/>
                    </a>
                </h2>
            </div>
        </div>
        <nav class="navbar navbar-default"  style="background-color: #009045">
            <div class="container-fluid">
                <div class="navbar-header">
                    <h2 style="color: white;">Módulo Votação</h2>
                </div>
            </div>
        </nav>
        
        <div class="container">
            <div class="col-lg-4"></div>
            <div class="col-lg-4">
                <div class="panel panel-default" style="margin-top:20%;">
                    <div class="panel-heading"><strong>Faça seu Login:</strong></div>
                    <div class="panel-body">
                        <form method="post" action="loginAutenticar">
                            <div class="form-group">
                                <label for="matricula">
                                    Usuário
                                </label>
                                <input type="matricula" name="matricula" class="novo_input-login novo_back-user" id="matricula" title="Preencha a sua matricula" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="senha"  title="Preencha a sua senha de acesso">
                                    Senha
                                </label>
                                <input type="password" name="senha" class="novo_input-login novo_back-senha" id="senha">
                            </div>
                            <button type="submit" class="btn btn-success btn-block novo_logar">
                                Entrar
                            </button>
                        </form>
                    </div>
                    <div class="panel-footer"></div>
                </div>
            </div>
            <div class="col-lg-4"></div>
        </div>


    </body>
</html>
