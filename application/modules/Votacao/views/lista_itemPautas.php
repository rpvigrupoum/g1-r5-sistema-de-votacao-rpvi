<section class="container">
    <ol class="breadcrumb">
        <li><a href="<?= base_url('reuniao') ?>">Reuniões</a></li>
        <li class="active">Itens de Pauta</li>
    </ol>

    <div style="float: right;">
        <strong style="margin: 0px 10px 0px 10px; font-size: 14px;">Status da Reunião</strong>

        <?php if ($reuniao['status'] == 'aberto'): ?>
            <div class="btn-group" id="status" data-toggle="buttons">
                <label value="<?= $reuniao['id_reuniao'] ?>" class="btn btn-default btn-on switch aberto active">
                    <input type="radio" checked="checked">Aberta</label>
                <label value="<?= $reuniao['id_reuniao'] ?>" class="btn btn-default btn-off switch fechado">
                    <input type="radio">Fechada</label>
            </div>
        <?php endif; ?>

        <?php if ($reuniao['status'] == 'fechado'): ?>
            <div class="btn-group" id="status" data-toggle="buttons">
                <label value="<?= $reuniao['id_reuniao'] ?>" class="btn btn-default btn-on switch aberto">
                    <input type="radio">Aberta</label>
                <label value="<?= $reuniao['id_reuniao'] ?>" class="btn btn-default btn-off switch fechado active">
                    <input type="radio" checked="checked">Fechada</label>
            </div>
        <?php endif; ?>
    </div>

    <h1>Itens de Pauta</h1>

    <table class="table table-striped table-bordered dataTable" id="example" style="width: 100%;">
        <thead>
            <tr>
                <th>Descrição</th>
                <th style="width: 250px">Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($item_pauta as $item_pautas) : ?>
                <tr>
                    <td>
                        <?= $item_pautas['descricao'] ?>
                    </td>
                    <td>
                        <form action="<?= base_url('opcaoVoto') ?>" method="post">
                            <button type="submit" class="btn btn-block btn-success" name="id" value="<?= $item_pautas['id_item_pauta'] ?>">Visualizar</button>
                        </form>
                    </td>
                </tr>
            </tbody>
        <?php endforeach; ?>
    </table>
</section>

<section class="container">
    <div class="col-lg-6">
        <?php if ($this->session->flashdata("success")) : ?>
            <p class="alert alert-success">Cadastro realizado com sucesso!</p>
        <?php endif ?>

        <?php if ($this->session->flashdata("danger")) : ?>
            <p class="alert alert-danger">Erro no cadastramento!</p>
        <?php endif ?>
    </div>
</section>