<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Votação em andamento</title>
        <!--        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
                <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>-->
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href='https://guri.unipampa.edu.br/public/themes/moder//css/estilo_menu.css' type="text/css" />
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <style>
            a, li, .form-group, th, .btn, p, .form-control, .panel-default{
                font-size: 14px;
            }
        </style>
    </head>
    <body>
        
        <div class="reset">    
            <div id="novo_tarja-topo"></div>
            <div id="novo_topo">
                <h1 id="novo_guri-marca">
                    <a href="https://guri.unipampa.edu.br/" title="GURI">
                        <img id="novo_marca" src="https://guri.unipampa.edu.br/public/themes/moder/imgs/logo_guri.png" alt="GURI"/>
                    </a>            
                </h1>
                <h2 id="novo_unipampa">
                    <a href="http://www.unipampa.edu.br" title="Acessar Portal da UNIPAMPA" target="_new" >
                        <img class="novo_marca-unipampa" src="https://guri.unipampa.edu.br/public/themes/moder/imgs/marca.png" alt="Acessar Portal da UNIPAMPA"/>
                    </a>
                </h2>
            </div>
        </div>
        <nav class="navbar navbar-default"  style="background-color: #009045">
            <div class="container-fluid">
                <div class="navbar-header">
                    <h2 style="color: white;">Modulo Votação</h2>
                </div>
            </div>
        </nav>

        <section class="container">
        <div>
        <table class="table table-striped table-hover">
            <tbody>
                <?php foreach ($opcoesVoto as $opcao) : ?>

                    <tr class="form-group">

                        <td><?= $opcao['descricao'] ?>
                            <input hidden type="text" value="<?= $opcao['descricao'] ?>" id="arrayOpcao[]" name="arrayOpcao[]">
                        </td>
                    </tr>

                <?php endforeach; ?>  
            </tbody>
        </table>
        </div>
                    <div class="form-group">
                <form id="formVotacao" method="post" name="formVotacao" action='<?= base_url('finalizaVotacao') ?>'>
                   
                    <input type="hidden" value='<?= $itemDePautaVotacao['id_item_pauta'] ?>' name="id_item_pauta" id="idItem"> <button type="submit" class="btn btn-primary float-right">Finalizar Votação</button>

                </form>
            </div>
        </section>
    </body>
</html>
