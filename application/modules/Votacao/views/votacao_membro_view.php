
    <ol class="breadcrumb">
        <li><a href="<?= base_url($url = '') ?>">Reunião</a></li>
        <li class="active">Itens de Pauta</li>
    </ol>

    <div style="float: right;">
        <strong style="margin: 0px 10px 0px 10px; font-size: 14px;">Abrir Reunião</strong>
        <div class="material-switch pull-right">
            <input id="someSwitchOptionSuccess" name="someSwitchOption001" type="checkbox"/>
            <label for="someSwitchOptionSuccess" class="label-success"></label>
        </div>
    </div>

    <h1>Votações abertas</h1>

  
    <table class="table table-striped table-bordered dataTable" id="example" style="width: 100%;">
        <thead>
            <tr>
                <th>Descrição</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($item_pauta as $item_pautas) : ?>
                <tr>
                    <td> <?= anchor($uri = "Votacao/ItemPauta/opcao?id={$item_pautas['id_item_pauta']}", $item_pautas['descricao']) ?></td>
                </tr>
            </tbody>
        <?php endforeach; ?>
    </table>
