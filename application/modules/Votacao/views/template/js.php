<script src="https://code.jquery.com/jquery-3.3.1.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script>
    $(".switch").click(function ()
    {
        var status;
        var $this = $(this);
        var request = new XMLHttpRequest();
        var data = new FormData();
        var id_reuniao = document.getElementsByClassName("aberto")[0].getAttribute("value");
        data.append('id_reuniao',id_reuniao);
        if ($this.hasClass('aberto')) {
            status = 'aberto';
        } else if ($this.hasClass('fechado')) {
            status = 'fechado';
        } else {
            status = null;
        }
        
        if(status != null){
            data.append('status',status);
            request.open("post", 'alterarStatus', true);
            request.send(data);
        }
        
    });
</script>
</body>
</html>