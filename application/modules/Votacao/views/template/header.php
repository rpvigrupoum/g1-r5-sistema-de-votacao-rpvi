<!DOCTYPE html>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href='https://guri.unipampa.edu.br/public/themes/moder//css/estilo_menu.css' type="text/css" />
        <title>Módulo Votação</title>
        <style>
            a, li, .form-group, th, .btn, p, td{
                font-size: 14px;
            }

            /* Switch button */
            .btn-default.btn-on.active{background-color: #5BB75B;color: white;}
            .btn-default.btn-off.active{background-color: #DA4F49;color: white;}

            .btn-default.btn-on-1.active{background-color: #006FFC;color: white;}
            .btn-default.btn-off-1.active{background-color: #DA4F49;color: white;}

            .btn-default.btn-on-2.active{background-color: #00D590;color: white;}
            .btn-default.btn-off-2.active{background-color: #A7A7A7;color: white;}

            .btn-default.btn-on-3.active{color: #5BB75B;font-weight:bolder;}
            .btn-default.btn-off-3.active{color: #DA4F49;font-weight:bolder;}

            .btn-default.btn-on-4.active{background-color: #006FFC;color: #5BB75B;}
            .btn-default.btn-off-4.active{background-color: #DA4F49;color: #DA4F49;}
            
            .novo_logar{
                width: 100%;
            }
            
            .btn-success{
                background-color: #009045;
            }
        </style>
    </head>
    <body>