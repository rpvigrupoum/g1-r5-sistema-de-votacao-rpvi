<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Sistema Votação</title>
        <!--        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
                <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>-->
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href='https://guri.unipampa.edu.br/public/themes/moder//css/estilo_menu.css' type="text/css" />
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <style>
            a, li, .form-group, th, .btn, p, .form-control, .panel-default{
                font-size: 14px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="jumbotron" style="margin-top:10%;">

                    <h3>Relatorio de votação</h3>


                    <section class="container">
                        <div>

                            <table class="table table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <td>Matrícula</td>
                                        <td>Opção Votada</td>
                                    </tr>


                                    <tr>
                                        <?php
                                        for ($i = 0; $i < sizeof($matriculas); $i++) {
                                            echo "<tr>";
                                            echo "<td>";
                                            echo $matriculas[$i];
                                            echo "</td>";
                                            echo "<td>";
                                            echo $descricoes[$i]['descricao'];
                                            echo" </td>";
                                            echo "</tr>";
                                        }
                                        ?>


                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <form action="<?= base_url('itemPautaIndex') ?>" method="post">
                                <button type="submit" class="btn btn-default" name="id" value="<?php echo $idReuniao ?>">Voltar para Reunião</button>    
                            </form>

                        </div>
                    </section>
                </div>
            </div>
            <div class="col-lg-3"></div>
        </div>
    </body>
</html>
