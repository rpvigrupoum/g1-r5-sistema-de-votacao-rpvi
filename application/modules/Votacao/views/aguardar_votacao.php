<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Login</title>
        <!--        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
                <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>-->
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href='https://guri.unipampa.edu.br/public/themes/moder//css/estilo_menu.css' type="text/css" />
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <style>
            a, li, .form-group, th, .btn, p, .form-control, .panel-default{
                font-size: 14px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="col-lg-4"></div>
            <div class="col-lg-4">
                <div class="jumbotron" style="margin-top:10%;">
                    Aguardando para votação...
                    <form id="form_aguardar" action="<?= base_url('votacao') ?>" method="post">
                        <input id="id_item_pauta" type="hidden" name="id_item_pauta" value="" />
                    </form>
                </div>
            </div>
            <div class="col-lg-4"></div>
        </div>

        <script>
            window.onload = function () {
                var source = new EventSource("votacaoAbertas");
                source.addEventListener("aguardarVotacao", function (event) {
                    console.log(event.data);redirecionar(event.data);
                }, false);

                function redirecionar(data) {
                    var array = data.split(";");
                    var arrayIds=[];
                    console.log(array.length);
                    for(i=0; i<array.length-1; i++){
                        var arrayE = array[i].split(',');
                         var id_reuniao = arrayE[0].replace(/[^0-9]/g, '');
                         var id_item_pauta = arrayE[1].replace(/[^0-9]/g, '');
                         var junto = []; 
                         junto['id_reuniao'] = id_reuniao;
                         junto['id_item_pauta'] = id_item_pauta;
                         arrayIds.push(junto);
                    }
                    console.log(arrayIds);
                       for(j=0; j<arrayIds.length; j++){
                            if(arrayIds[j]['id_reuniao'] == <?= $id_reuniao ?>){
                           document.getElementById("id_item_pauta").setAttribute("value", arrayIds[j]['id_item_pauta']);
                           document.getElementById("form_aguardar").submit();
                            }
                       }
                       
                   }
                
            };


        </script>
    </body>
</html>
