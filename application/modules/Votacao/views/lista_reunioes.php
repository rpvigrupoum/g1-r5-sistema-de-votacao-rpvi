<section class="container">
    <ol class="breadcrumb">
        <!--<li><a href="">Início</a></li>-->
        <li class="active">Reuniões</li>
    </ol>


    <h1>Reuniões - Moderador</h1>

    <table class="table table-striped table-bordered dataTable" id="example" style="width: 100%;">
        <thead>
            <tr>
                <th>Descrição</th>
                <th style="width: 250px">Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($reuniao as $reunioes) : ?>

                <tr>                       
                    <td>
                        <?= $reunioes['descricao'] ?>
                    </td>
                    <td>
                        <form action="<?= base_url('itemPautaIndex') ?>" method="post">
                            <button type="submit" class="btn btn-block btn-success" name="id" value="<?= $reunioes['id_reuniao'] ?>">Visualizar</button>
                        </form>
                    </td>   
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <script>
        window.onload = function () {
            var source = new EventSource("visualizarReunioesAbertas");
            source.addEventListener("reunioesAbertas", function (event) {
                desativarTodosFechados();
                ativarAbertos(event.data);
            }, false);

            function ativarAbertos(data) {
                var arrayIds = data.split(",");
                for (i = 0; i < arrayIds.length; i++) {
                    var numsStr = arrayIds[i].replace(/[^0-9]/g, '');
                    if (document.getElementById(numsStr) != null) {
                        document.getElementById(numsStr).disabled = false;
                    }
                }
            }

            function desativarTodosFechados() {
                var botoes = document.getElementsByName("id_reuniao");

                for (i = 0; i < botoes.length; i++) {
                    botoes[i].disabled = true;

                }
            }
        };


    </script>

    <h1>Reuniões - Conselheiro</h1>

    <table class="table table-striped table-bordered dataTable" id="example" style="width: 100%;">
        <thead>
            <tr>
                <th>Descrição</th>
                <th style="width: 250px">Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($reunioesComoConselheiros as $reunioesComoConselheiro) : ?>
                <tr>                       
                    <td>
                        <?= $reunioesComoConselheiro['descricao'] ?>
                    </td>
                    <td>
                        <form action="<?= base_url('aguardarVotacao') ?>" method="post">
                            <?php if ($reunioesComoConselheiro['status'] === 'fechado') : ?>
                                <button type="submit" class="btn btn-block" name="id_reuniao" id="<?= $reunioesComoConselheiro['id_reuniao'] ?>" disabled="true" value="<?= $reunioesComoConselheiro['id_reuniao'] ?>">Entrar</button>
                            <?php elseif ($reunioesComoConselheiro['status'] === 'aberto') : ?>
                                <button type="submit" class="btn btn-block btn-success btn-disabled" name="id_reuniao" disabled="false" id="<?= $reunioesComoConselheiro['id_reuniao'] ?>" value="<?= $reunioesComoConselheiro['id_reuniao'] ?>">Entrar</button>
                            <?php endif; ?>
                        </form>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</section>