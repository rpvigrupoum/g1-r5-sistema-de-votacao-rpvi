<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Módulo Votação</title>
        <!--        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
                <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>-->
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href='https://guri.unipampa.edu.br/public/themes/moder//css/estilo_menu.css' type="text/css" />

        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <!------ Include the above in your HEAD tag ---------->
<!--        <link rel="stylesheet" type="text/css" href="<?= base_url($uri = 'ci/assets/css/bootstrap.css') ?>">-->
        <style>
            a, li, .form-group, th, .btn, p, .form-control{
                font-size: 14px;
            }

            .btn-success{
                background-color: #009045;
            }
        </style>
    </head>
    <body>

        <div class="reset">    
            <div id="novo_tarja-topo"></div>
            <div id="novo_topo">
                <h1 id="novo_guri-marca">
                    <a href="https://guri.unipampa.edu.br/" title="GURI">
                        <img id="novo_marca" src="https://guri.unipampa.edu.br/public/themes/moder/imgs/logo_guri.png" alt="GURI"/>
                    </a>            
                </h1>
                <h2 id="novo_unipampa">
                    <a href="http://www.unipampa.edu.br" title="Acessar Portal da UNIPAMPA" target="_new" >
                        <img class="novo_marca-unipampa" src="https://guri.unipampa.edu.br/public/themes/moder/imgs/marca.png" alt="Acessar Portal da UNIPAMPA"/>
                    </a>
                </h2>
            </div>
        </div>
        <nav class="navbar navbar-default"  style="background-color: #009045">
            <div class="container-fluid">
                <div class="navbar-header">
                    <h2 style="color: white;">Modulo Votação</h2>
                </div>
            </div>
        </nav>

        <section class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">
                        <!--<li><a href="">Início</a></li>-->
                        <li><a href="<?= base_url('reuniao') ?>">Reunião</a></li>
                        <li class="active">Opções de Voto</li>
                    </ol>
                </div>

                <div class="col-lg-3"></div>
                <div class="col-lg-6">

                    <div class="form-group">

                        <?= $itemDePauta['descricao'] ?>
                    </div>

                    <div class="form-group">
                        <?php
                        if ($itemDePauta['tipoVotacao'] == 2) {
                            echo '<input type="radio" onclick="javascript:yesnoCheck();" name="yesno" id="simplesCheck">
                        Encaminhamento Simples
                        <input checked="true" type="radio" onclick="javascript:yesnoCheck();" name="yesno" id="personalizadoCheck">
                        Encaminhamento Personalizado ';
                        } else {
                            echo '<input checked="true" type="radio" onclick="javascript:yesnoCheck();" name="yesno" id="simplesCheck">
                        Encaminhamento Simples
                        <input  type="radio" onclick="javascript:yesnoCheck();" name="yesno" id="personalizadoCheck">
                        Encaminhamento Personalizado ';
                        }
                        ?>
                    </div>

                    <div id="simples"  class="form-group multiple-form-group input-group" method="post" action="cadastrarSimples" style="<?php
                    if ($itemDePauta['tipoVotacao'] == 1)
                        echo'display: block;';
                    else {
                        echo'display: none;';
                    };
                    ?>><">
                        <div class="form-group">
                            <input type="hidden" class="form-control" value="<?= $itemDePauta['id_item_pauta'] ?>" name="idItem" id="idItem"/>
                        </div>
                        <div class="form-group">
                            <ul class="list-group">
                                <li class="list-group-item">A favor</li>
                                <li class="list-group-item">Contrário</li>
                                <li class="list-group-item">Abstenção</li>
                            </ul>
                      
                            <button id="botaoSimples" name="botaoSimples" onclick="salvarSimples()" value="<?= $itemDePauta['id_item_pauta'] ?>" class="btn btn-success btn-block">
                                Salvar
                            </button> 
                        </div>

                        </div>

                        <div id="personalizado"  class="form-group multiple-form-group input-group" method="post" action="cadastrarPerso" style="<?php
                        if ($itemDePauta['tipoVotacao'] == 2)
                            echo'display: block;';
                        else {
                            echo'display: none;';
                        };
                        ?>><">

                            <input type="hidden" value="<?= $itemDePauta['id_item_pauta'] ?>" name="idItem" id="idItem">
                            <div class="form-inline">
                                <label>Descrição:</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="opcaoVoto" id="opcaoVoto" autocomplete="Off">
                                </div>
                                <span>
                                    <button  type='button' onclick="adicionar()" class='btn btn-success'>Adicionar</button>
                                </span>
                                  <!--<input style="margin-left: 10%" type="checkbox" name="meucheckbox" value="Segundo Turno"> Segundo Turno-->
                            </div>
                            <!--<button type="button" onclick="adicionar()">Adicionar</button>-->


                            <div class="form-group">
                                <input id="arrayOpcao" disabled type="text" class="form-control" value="Abstenção"><br>
                                <div class='form-group'>



                                    <table class="table table-striped table-hover">
                                        <tbody>
                                            <?php foreach ($opcoesVoto as $opcao) : ?>

                                                <tr class="form-group">

                                                    <td><?= $opcao['descricao'] ?>
                                                        <input hidden type="text" value="<?= $opcao['descricao'] ?>" id="arrayOpcao[]" name="arrayOpcao[]">
                                                    </td>
                                                    <td><button type='button' class='btn btn-danger btn-block'>Deletar</button></td>
                                                </tr>

                                            <?php endforeach; ?>  
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="form-group" style="margin-top:-15px;">
                                <br>
                                <button id="botaoPerso" onclick="salvar()" value="<?= $itemDePauta['id_item_pauta'] ?>" class="btn btn-success btn-block">
                                    Salvar
                                </button> 
                            </div>
                        </div>

                        <!--</div>-->
                        <div class="form-group">
                            <form id="formVotacao" method="post" name="formVotacao" action="iniciaVotacao">
                                <input type="hidden" value="<?= $itemDePauta['id_item_pauta'] ?>" name="idItem" id="idItem"> 
                                <button type="submit" class="btn btn-success btn-block">
                                    Iniciar Votação
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-3"></div>
                    </section>

                    <section class="container">
                        <div class="col-lg-6">
                            <?php if ($this->session->flashdata("success")) : ?>
                                <p class="alert alert-success">Alterações salvas com sucesso!</p>
                            <?php endif ?>

                            <?php if ($this->session->flashdata("danger")) : ?>
                                <p class="alert alert-danger">Erro no cadastramento!</p>
                            <?php endif ?>
                        </div>
                    </section>

                    <script>
                        function yesnoCheck() {
                            if (document.getElementById('simplesCheck').checked) {
                                document.getElementById('simples').style.display = 'block';

                            } else {
                                document.getElementById('simples').style.display = 'none';
                            }

                            if (document.getElementById('personalizadoCheck').checked) {
                                document.getElementById('personalizado').style.display = 'block';

                            } else {
                                document.getElementById('personalizado').style.display = 'none';
                            }

                        }
                    </script>
                    <script>
                        $(document).ready(function () {
                            $(".alert-success").fadeOut(8000);
                            $(".alert-danger").fadeOut(8000);
                        });
                    </script>
                    <script>
                        function adicionar() {

                            var opcao = document.getElementById("opcaoVoto").value;
                            var tabela = document.querySelector(".table tbody");
                            if (validaCampoVazio(opcao) && validaCampoIgual(opcao) == true) {
                                var tr = document.createElement('tr');
                                tr.className = 'form-group';
                                tr.id = "event";


                                var td3 = document.createElement('td');
                                var textoOpcao = document.createTextNode(opcao);
                                td3.appendChild(textoOpcao);
                                tr.appendChild(td3);
                                var td4 = document.createElement('td');
            //                    var td5 = document.createElement('td');






                                var InputOpcao = document.createElement('input');
                                InputOpcao.type = "text";
                                InputOpcao.hidden = true;
                                InputOpcao.className = "form-control";
                                InputOpcao.name = "arrayOpcao[]";
                                InputOpcao.id = "arrayOpcao[]";
                                InputOpcao.setAttribute("value", opcao);
                                td3.appendChild(InputOpcao);
                                tr.appendChild(td3);


                                var BotaoDelete = document.createElement('button');
                                BotaoDelete.type = "button";
                                BotaoDelete.className = "btn btn-danger";

                                var TextoBotao = document.createTextNode("Deletar");
                                BotaoDelete.appendChild(TextoBotao);


                                td4.appendChild(BotaoDelete);
                                tr.appendChild(td4);

                                document.querySelector(".table tbody").appendChild(tr);

                            }
                        }


                    </script>
                    <script>
                        function validaCampoVazio(opcao) {
                            if (opcao == "" || opcao == null) {

                                alert("O campo não foi preenchido!");
                                return false;
                            } else {
                                return true;
                            }
                        }
                    </script>
                    <script>
                        function validaCampoIgual(opcao) {
                            arrayOpcao = document.getElementsByName("arrayOpcao[]");
                            igual = true;
                            for (var i = 0; i < arrayOpcao.length; i++) {
                                if (arrayOpcao[i].value == opcao) {
                                    igual = false;
                                    alert("A opção já existe!");
                                }
                            }
                            return igual;
                        }
                    </script>
                    <script>
                        (function ($) {
                            $(function () {


                                var removeFormGroup = function (event) {
                                    event.preventDefault();
                                    var $formGroup = $(this).closest('.form-group');
                                    $formGroup.remove();
                                };
                                $(document).on('click', '.btn-danger', removeFormGroup);
                            });
                        })(jQuery);
                    </script>
                    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> 
                    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script> 
                    <script src="https://getbootstrap.com/dist/js/bootstrap.min.js"></script>
                    <script type="text/javascript">
                        $('tbody').sortable();
                    </script>
                    <script>
                        function salvar()
                        {

                            var request = new XMLHttpRequest();
                            var data = new FormData();
                            var idItemPauta = document.getElementById("botaoPerso").getAttribute("value");
                            var descricoes = document.getElementsByName("arrayOpcao[]");
                            var valueDescricoes = [];
                            for (i = 0; i < descricoes.length; i++) {
                                valueDescricoes.push(descricoes[i].getAttribute("value"));

                            }

                            data.append('idItem', idItemPauta);
                            data.append('arrayOpcao[]', valueDescricoes);

                            request.open("post", 'cadastrarPerso', true);
                            request.send(data);
                            console.log(request.responseText);
                            request.onreadystatechange = function () {
                                if (this.readyState === 4 && this.status === 200) {
                                    alert("Opções salvas!");

                                }

                            }


                        }

                    </script>
                    <script>
                        function salvarSimples()
                        {

                            var request = new XMLHttpRequest();
                            var data = new FormData();
                            var idItemPauta = document.getElementById("botaoSimples").getAttribute("value");

                            data.append('idItem', idItemPauta);
                            request.open("post", 'cadastrarSimples', true);
                            request.send(data);
                            request.onreadystatechange = function () {
                                if (this.readyState === 4 && this.status === 200) {
                                    alert("Opções salvas!");

                                }

                            }


                        }

                    </script>
                    </body>
                    </html>