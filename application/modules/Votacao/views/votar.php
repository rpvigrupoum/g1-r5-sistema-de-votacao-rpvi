<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Sistema Votação</title>
        <!--        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
                <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>-->
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href='https://guri.unipampa.edu.br/public/themes/moder//css/estilo_menu.css' type="text/css" />
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <style>
            a, li, .form-group, th, .btn, p, .form-control, .panel-default{
                font-size: 14px;
            }
            
                        .btn-success{
                background-color: #009045;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="jumbotron" style="margin-top:10%;">
                    <h2> 
                        
                     <?= $ItemPauta['descricao']?>
                    </h2>
                    <table class="table table-striped table-bordered dataTable" id="example" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Descrição</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($opcoes as $opcao) : ?>

                                <tr>                       
                                    <td>
                                        <?= $opcao['descricao'] ?>
                                    </td>
                                    <td>
                                        <form action="<?= base_url('votar') ?>" method="post">
                                            <input name="id_item_pauta" type='hidden' value="<?= $ItemPauta['id_item_pauta'] ?>"/>
                                            <button onclick='alerta()' type="submit" class="btn btn-success btn-block" name="id_opcao_voto" value="<?= $opcao['id_opcao_voto'] ?>">Votar</button>
                                        </form>
                                    </td>   
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-3"></div>
        </div>
        <script>
            function alerta(){
                alert('Voto registrado!');
            }
        </script>
    </body>
</html>
