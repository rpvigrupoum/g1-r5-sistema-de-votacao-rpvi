<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Opções de Voto</title>
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href='https://guri.unipampa.edu.br/public/themes/moder//css/estilo_menu.css' type="text/css" />
        <!------ Include the above in your HEAD tag ---------->
        <link rel="stylesheet" type="text/css" href="<?= base_url($uri = 'ci/assets/css/bootstrap.css') ?>">
        <style>
            a, li, .form-group, th, .btn, p, .form-control{
                font-size: 14px;
            }
        </style>
    </head>
    <body>

        <div class="reset">    
            <div id="novo_tarja-topo"></div>
            <div id="novo_topo">
                <h1 id="novo_guri-marca">
                    <a href="https://guri.unipampa.edu.br/" title="GURI">
                        <img id="novo_marca" src="https://guri.unipampa.edu.br/public/themes/moder/imgs/logo_guri.png" alt="GURI"/>
                    </a>            
                </h1>
                <h2 id="novo_unipampa">
                    <a href="http://www.unipampa.edu.br" title="Acessar Portal da UNIPAMPA" target="_new" >
                        <img class="novo_marca-unipampa" src="https://guri.unipampa.edu.br/public/themes/moder/imgs/marca.png" alt="Acessar Portal da UNIPAMPA"/>
                    </a>
                </h2>
            </div>
        </div>
        <nav class="navbar navbar-default"  style="background-color: #009045">
            <div class="container-fluid">
                <div class="navbar-header">
                    <h2 style="color: white;">Modulo Votação</h2>
                </div>
            </div>
        </nav>

        <section class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">
                        <!--<li><a href="">Início</a></li>-->
                        <li><a href="<?= base_url($url = './') ?>">Itens de Pauta</a></li>
                        <li class="active">Opções de Voto</li>
                    </ol>
                </div>
                <table id="table" class="table">
                    <?php foreach ($opcoesVoto as $opcao) : ?>

                        <tr class="form-group">
                            <td><input type="text" disabled class="form-control"  name="arrayOpcao[]" value="<?= $opcao['descricao'] ?>"></td>
                            <td><button type='button' class='btn btn-success'>Votar</button></td>
                        </tr>

                    <?php endforeach; ?>  
                </table>
            </div>
        </section>

        <section class="container">
            <div class="col-lg-6">
                <?php if ($this->session->flashdata("success")) : ?>
                    <p class="alert alert-success">Cadastro realizado com sucesso!</p>
                <?php endif ?>

                <?php if ($this->session->flashdata("danger")) : ?>
                    <p class="alert alert-danger">Erro no cadastramento!</p>
                <?php endif ?>
            </div>
        </section>

    </body>
</html>