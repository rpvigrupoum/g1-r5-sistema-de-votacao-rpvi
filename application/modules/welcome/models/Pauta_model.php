<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pauta_model
 *
 * @author JPiagetti
 */
class Pauta_model extends CI_Model {

    private $pauta;
    private $descricao;
    private $id_reuniao;

    public function __construct() {
        parent::__construct();
    }

    function getPauta($id) {
        return $this->db->get_where('item_pauta', array('id_reuniao' => $id))->row_array();
    }

    function getDescricao() {
        return $this->descricao;
    }

    function setPauta($pauta) {
        $this->pauta = $pauta;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    function setId_reuniao($id_reuniao) {
        $this->id_reuniao = $id_reuniao;
    }
    public function getAllPautas(){
        return $this->db->get('item_pauta')->result();
    }
}
