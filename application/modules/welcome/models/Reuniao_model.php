<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Reuniao_model
 *
 * @author JPiagetti
 */
class Reuniao_model extends CI_Model {

    private $moderador;
    private $descricao;
    private $idReuniao;
    private $dataInicio;
    private $dataFim;
    private $status;

    function getDescricao() {
        return $this->descricao;
    }

    function getIdReuniao() {
        return $this->idReuniao;
    }

    function getDataInicio() {
        return $this->dataInicio;
    }

    function getDataFim() {
        return $this->dataFim;
    }

    function getStatus() {
        return $this->status;
    }

    function setModerador($moderador) {
        $this->moderador = $moderador;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    function setIdReuniao($idReuniao) {
        $this->idReuniao = $idReuniao;
    }

    function setDataInicio($dataInicio) {
        $this->dataInicio = $dataInicio;
    }

    function setDataFim($dataFim) {
        $this->dataFim = $dataFim;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    public function __construct() {
        parent::__construct();
    }

    function getModerador() {
        return $this->moderador;
    }

    function getReuniaoId($id) {
        $retorno = null;
        $this->db->select('id_reuniao');
        $this->db->from('reuniao');
        $retorno = $this->db->get_where('id_reuniao', $id);
        $this->idReuniao = $retorno;
        return $retorno;
    }

    public function getAll() {
        return $this->db->from('reuniao')->get()->result();
    }


    
}
