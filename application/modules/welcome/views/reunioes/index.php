<html>
    <head>
        <link rel="stylesheet" href=" <?php echo base_url($uri = "css/bootstrap.css") ?> ">
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900' rel='stylesheet' type='text/css'/>  
        <link rel="icon" href="https://guri.unipampa.edu.br/public/themes/moder//imgs/favicon-guri16.png" type="image/png"/>
        <!-- css global -->
        <link rel="stylesheet" href='https://guri.unipampa.edu.br/public/themes/moder//css/estilo_menu.css' type="text/css" />

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>





        <div class="reset">    
            <div id="novo_tarja-topo"></div>
            <div id="novo_topo">
                <h1 id="novo_guri-marca">
                    <a href="https://guri.unipampa.edu.br/" title="GURI">
                        <img id="novo_marca" src="https://guri.unipampa.edu.br/public/themes/moder/imgs/logo_guri.png" alt="GURI"/>
                    </a>            
                </h1>
                <h2 id="novo_unipampa">
                    <a href="http://www.unipampa.edu.br" title="Acessar Portal da UNIPAMPA" target="_new" >
                        <img class="novo_marca-unipampa" src="https://guri.unipampa.edu.br/public/themes/moder/imgs/marca.png" alt="Acessar Portal da UNIPAMPA"/>
                    </a>
                </h2>
            </div>
        </div>
        <script>
            // Extra small devices (portrait phones, less than 576px)
            @media (max - width: 575.98px) { ... }

            // Small devices (landscape phones, less than 768px)
            @media (max - width: 767.98px) { ... }

            // Medium devices (tablets, less than 992px)
            @media (max - width: 991.98px) { ... }

            // Large devices (desktops, less than 1200px)
            @media (max - width: 1199.98px) { ... }

            // Extra large devices (large desktops)
            // No media query since the extra-large breakpoint has no upper bound on its width
        </script>
















        <nav class="navbar navbar-inverse" style="background-color: #009045">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <h3>Módulo de Votação</h3>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                  
                    <ul class="nav navbar-nav navbar-right">
                     
                        <li><a href="#">Reuniões</a></li>
                    </ul>
                </div>
            </div>
        </nav>




        <div class="container">


            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-body"><h1>Menu </h1></div>

                
                <table class="table">
                         
                     <tr>
                         <th>ID</th>
                          <th>Descrição</th>  
                     </tr>
                       <?php foreach ($reunioes as $reuniao) : ?>
                     <tr>
                          <td><?=$reuniao['id_reuniao']?></td>
                          <td><?=$reuniao['descricao']?></td>
                          <td><a>Abrir </a></td>
                     </tr>
                     <?php endforeach ?>
                     </table>
            </div>


        </div>

    </body>





</html>