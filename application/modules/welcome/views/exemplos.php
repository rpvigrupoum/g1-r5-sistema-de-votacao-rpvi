<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">



        <title>Reuniões</title>
        <script>

            /*
             * Função que carrega após o DOM estiver carregado.
             * Como estou usando o ajaxForm no formulário, é aqui que eu o configuro.
             * Basicamente somente digo qual função será chamada quando os dados forem postados com sucesso.
             * Se o retorno for igual a 1, então somente recarrego a janela.
             */
            $(function () {
                $('#formulario_reunioes').ajaxForm({
                    success: function (data) {
                        if (data === 1) {

                            //se for sucesso, simplesmente recarrego a página. Aqui você pode usar sua imaginação.
                            document.location.href = document.location.href;

                        }
                    }
                });
            });

            //Aqui eu seto uma variável javascript com o base_url do CodeIgniter, para usar nas funções do post.
            var base_url = "http://localhost/g1-r5-sistema-de-votacao-rpvi";

            /*
             *	Esta função serve para preencher os campos da reuniao na janela flutuante
             * usando jSon.  
             */
            function carregarPautasJSon(id_reuniao) {
                $.post(base_url + '/index.php/Welcome/dados_reuniao', {
                    id_reuniao: id_reuniao
                }, function (data) {
                    // $('#reuniao').val(data.id_reuniao);
                    $('#descricao').val(data.descricao);
                    $('#id_reuniao').val(data.id_reuniao);//aqui eu seto a o input hidden com o id da reuniao. Em cada tela aberta, eu seto o id da reuniao. 
                }, 'json');
            }

            function janelaMostrarPautas(id_reuniao) {

                //antes de abrir a janela, preciso carregar os dados da reuniao e preencher os campos dentro do modal
                carregarPautasJSon(id_reuniao);
                $('#modalMostrarPautas').modal('show');
            }

        </script>
    </head>

    <body>

        <div class="container">

            <div class="panel panel-primary">
                <div class="panel-heading">

                </div>
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <th>ID Reunião</th>
                            <th>Descrição</th>
                            <th>Moderador</th>
                            <td> </td>
                        </tr>
                        <?php foreach ($reunioes as $reuniao): ?>
                            <tr>
                                <td><?= $reuniao->id_reuniao ?></td>
                                <td><?= $reuniao->descricao ?></td>
                                

                                <!--<td> <a href="javascript:;" onclick="janelaMostrarPautas(<?php //echo $reuniao->id_reuniao ?>)"  data-target="#modalMostrarPautas"<i class="fa fa-pencil "/>Moderar</a></td>-->
                                <!--MODIFICAÇÂO -->
                                <td> <?php anchor($uri="Reuniao/ItemPauta?id={$reuniao['id_reuniao']},Moderar")?></td>
                            </tr>
                        <?php endforeach; ?>
                </div>
            </div>		

        </div><!-- /.container -->
        <div class="modal fade bs-example-modal-lg" id="modalMostrarPautas" >
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title"> Abrir Reunião</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>

                    </div>
                    <div class="modal-body">
                        <form role="form" method="post" action="<?php echo base_url('/index.php/Welcome/dados_reuniao') ?>" id="formulario_reunioes">
                            <div class="form-group">
                                <label for="reuniao">Reunião</label>
                                <label for="reuniao"><?php echo $reuniao->descricao; ?></label>
                               
                                
                            </div>
                            <div class="form-group">
                                <hr>
                                <label for="id_item_pauta" >Itens de pauta:</label>
                               <!-- <input type="text" class="form-control" id="descricao" name="descricao">-->
                                
                                <div id="listaPautasdaReuniao">
                                    <hr>
                                    <p>* Item 1 </p>
                                    <hr> 
                                    <p>* Item  2</p>
                                    <hr>
                                    <p>* Item  3</p>
                                </div>
                       
                            </div>
                            <input type="hidden" name="id_reuniao" id="id_reuniao" value="" />
                            
                        </form>	    
                        

                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" onclick="$('#formulario_reunioes').submit()">Abrir Reunião</button>
                        <button type="button" class="btn btn-danger" style="display: none" onclick="$('#formulario_reunioes').submit()">fechar Reunião</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->  
    </body>
</html>