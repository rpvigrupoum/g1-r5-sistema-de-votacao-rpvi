<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
       
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


        <!--template guri>
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900' rel='stylesheet' type='text/css'/>  
        <link rel="icon" href="https://guri.unipampa.edu.br/public/themes/moder//imgs/favicon-guri16.png" type="image/png"/>
        <!-- css global -->

        <link rel="stylesheet" href='https://guri.unipampa.edu.br/public/themes/moder//css/estilo_menu.css' type="text/css" />
    </head>
    <body>

        <div class="reset">    
            <div id="novo_tarja-topo"></div>
            <div id="novo_topo">
                <h1 id="novo_guri-marca">
                    <a href="https://guri.unipampa.edu.br/" title="GURI">
                        <img id="novo_marca" src="https://guri.unipampa.edu.br/public/themes/moder/imgs/logo_guri.png" alt="GURI"/>
                    </a>            
                </h1>
                <h2 id="novo_unipampa">
                    <a href="http://www.unipampa.edu.br" title="Acessar Portal da UNIPAMPA" target="_new" >
                        <img class="novo_marca-unipampa" src="https://guri.unipampa.edu.br/public/themes/moder/imgs/marca.png" alt="Acessar Portal da UNIPAMPA"/>
                    </a>
                </h2>
            </div>
        </div>
        <nav class="navbar navbar-default"  style="background-color: #009045">
            <div class="container-fluid">
                <div class="navbar-header">
                    <h2><?php echo $title ?></h2>
                </div>
            </div>
        </nav>
    </body>
</html>
