<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Testes
 *
 * @author JPiagetti
 */
class Testes extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('unit_test');
    }

    public function retornaReuniao() {
        $this->load->model('Reuniao_model');
        $reuniao = $this->Reuniao_model();
        $test = $reuniao->getDescricao(1);
        $expected_result = 'reunião 1';
        $test_name = 'retornaReuniao';
        $str_template = 'ITEM | DESCRIÇÃO' . PHP_EOL . '{rows}{item} | {result}' . PHP_EOL . '{/rows}';
        $this->unit->set_template($str_template);
        echo $this->unit->run($test, $expected_result, $test_name);
    }

}
